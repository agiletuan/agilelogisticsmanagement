﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    [Table(nameof(LinhVucKinhDoanh))]
    public class LinhVucKinhDoanh
    {
        public LinhVucKinhDoanh()
        {
            this.DoiTacs = new HashSet<DoiTac>();
        }
        public int Id { get; set; }
        [Display(Name = "Tên")]
        public string Ten { get; set; }
        public ICollection<DoiTac> DoiTacs { get; set; }
    }

}
