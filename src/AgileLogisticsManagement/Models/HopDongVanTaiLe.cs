﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    /// <summary>
    /// Hợp đồng vận tải lẻ (đơn hàng lẻ; là hợp đồng thường xuyên)
    /// </summary>
    [Table(nameof(HopDongVanTaiLe))]
    public class HopDongVanTaiLe
    {
        public HopDongVanTaiLe()
        {
            this.DonHangGheps = new HashSet<DonHangGhep>();
        }
        public int Id { get; set; }
        /// <summary>
        /// Đối tác: ở đây thường là khách hàng.
        /// </summary>
        public int IdDoiTac { get; set; }
        public DoiTac DoiTac { get; set; }


        /// <summary>
        /// Loại hàng hóa.
        /// </summary>
        public int IdLoaiHangHoa { get; set; }
        public LoaiHangHoa LoaiHangHoa { get; set; }
        /// <summary>
        /// Các đơn hàng ghép.
        /// Quan hệ n-n với HopDongVanTai, nên phải tách ra thành n-1 & 1-n.
        /// </summary>
        public ICollection<DonHangGhep> DonHangGheps { get; set; }


        //Không cần hợp đồng.
        /// <summary>
        /// Tệp scan hợp đồng (dạng  pdf hoặc hình ảnh)
        /// </summary>
        //public string FileHopDong { get; set; }
        /// <summary>
        /// Nội dung email của hợp đồng.
        /// Cho định dạng Html.
        /// </summary>
        //public string NoiDungEmail { get; set; }

        ///Không cần giá trị hợp đồng, vì giá trị đưa vào luôn hợp đồng chính.
        //public double GiaTriHopDong { get; set; }

        ///Không cần giá lộ trình giao, vì giá trị đưa vào luôn hợp đồng chính.
        ///// <summary>
        ///// Các mục trên lộ trình giao hàng.
        ///// </summary>
        //public ICollection<LoTrinhGiao> LoTrinhGiaos { get; set; }

    }

}
