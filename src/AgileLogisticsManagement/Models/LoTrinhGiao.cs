﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    [Table(nameof(LoTrinhGiao))]
    public class LoTrinhGiao
    {
        public int Id { get; set; }
        public DateTime ThoiGian { get; set; }
        public string DiaDiem { get; set; }
        public string NguoiLienHe { get; set; }
        public string SoDienThoai { get; set; }
        public string NguoiNhan { get; set; }
        public int IdHopDongVanTai { get; set; }
        public HopDongVanTai HopDongVanTai { get; set; }
    }

}
