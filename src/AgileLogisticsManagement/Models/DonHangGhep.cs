﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    [Table(nameof(DonHangGhep))]
    public class DonHangGhep
    {
        public int Id { get; set; }
        public int IdHopDongVanTai { get; set; }
        public HopDongVanTai HopDongVanTai { get; set; }
        public int IdHopDongVanTaiLe { get; set; }
        public HopDongVanTaiLe HopDongVanTaiLe { get; set; }
    }

}
