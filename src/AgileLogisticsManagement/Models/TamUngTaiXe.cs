﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    [Table(nameof(TamUngTaiXe))]
    public class TamUngTaiXe
    {
        public TamUngTaiXe()
        {
            this.ThoiGian = DateTime.Now;
        }
        public int Id { get; set; }
        [Display(Name = "Tài xế")]
        public int IdTaiXe { get; set; }
        [Display(Name = "Tài xế")]
        public TaiXe TaiXe { get; set; }
        [Display(Name = "Số tiền")]
        public double SoTien { get; set; }

        [Display(Name = "Thời gian tạm ứng")]
        [Required(ErrorMessage = "Hãy chọn thời gian tạm ứng.")]
        [DataType(DataType.DateTime)]
        public DateTime ThoiGian { get; set; }

        /// <summary>
        /// Tạm ứng có thể kèm với hợp đồng hoặc không.        
        /// </summary>
        [Display(Name = "Theo hợp đồng vận tải")]
        public int IdHopDongVanTai { get; set; } = 0;
    }

}
