﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    [Table(nameof(LoaiChucVu))]
    public class LoaiChucVu
    {
        public LoaiChucVu()
        {
            this.NhanViens = new HashSet<NhanVien>();
        }
        
        public int Id { get; set; }
        [Display(Name = "Tên")]
        public string Ten { get; set; }
        public ICollection<NhanVien> NhanViens { get; set; }
    }

}
