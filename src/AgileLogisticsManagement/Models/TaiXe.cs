﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    [Table(nameof(TaiXe))]
    public class TaiXe
    {
        public TaiXe()
        {
            this.TamUngTaiXes = new HashSet<TamUngTaiXe>();
            this.HopDongVanTais = new HashSet<HopDongVanTai>();
        }
        public int Id { get; set; }
        [Display(Name = "Tên")]
        public string Ten { get; set; }
        [Display(Name = "Mã số")]
        public string MaSo { get; set; }
        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }
        [Display(Name = "Sđt")]
        public string SoDienThoai { get; set; }
        [Display(Name = "Loại giấy phép")]
        public int IdLoaiGiayPhepLaiXe { get; set; }
        [Display(Name = "Loại giấy phép")]
        public LoaiGiayPhepLaiXe LoaiGiayPhepLaiXe { get; set; }

        public ICollection<TamUngTaiXe> TamUngTaiXes { get; set; }
        public ICollection<HopDongVanTai> HopDongVanTais { get; set; }

    }

}
