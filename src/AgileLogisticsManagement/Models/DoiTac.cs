﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    /// <summary>
    /// Đối tác: có thể một đơn vị, cá nhân vừa là khách hàng, vừa là nhà cung cấp sản phẩm.
    /// Phòng trường hợp sau này bổ sung module kho bãi.    /// 
    /// </summary>
    [Table(nameof(DoiTac))]
    public class DoiTac
    {
        public DoiTac()
        {
            this.HopDongVanTais = new HashSet<HopDongVanTai>();
            this.HopDongVanTaiLes = new HashSet<HopDongVanTaiLe>();

        }
        public int Id { get; set; }
        [Display(Name = "Mã số")]
        public string MaSo { get; set; }
        [Display(Name = "Tên")]
        public string Ten { get; set; }
        public string CMND { get; set; }
        [Display(Name = "Sđt")]
        public string SoDienThoai { get; set; }
        [Display(Name = "MST")]
        public string MaSoThue { get; set; }

        [Display(Name = "Lĩnh vực kd")]
        public int IdLinhVucKinhDoanh { get; set; }
        [Display(Name = "Lĩnh vực kd")]
        public LinhVucKinhDoanh LinhVucKinhDoanh { get; set; }

        /// <summary>
        /// Cho nhập chuỗi.
        /// </summary>
        [Display(Name = "Loại hàng hóa")]
        public string LoaiHangHoaVanTai { get; set; }
        [Display(Name = "Đ/c")]
        public string DiaChi { get; set; }
        [Display(Name = "Thông tin xuất hóa đơn")]
        public string ThongTinXuatHoaDon { get; set; }
        [Display(Name = "Người liên hệ")]
        public string NguoiLienHe { get; set; }
        /// <summary>
        /// Là cá nhân chứ ko phải tổ chức/công ty
        /// </summary>
        [Display(Name = "Là cá nhân")]
        public bool IsCaNhan { get; set; }

        public ICollection<HopDongVanTai> HopDongVanTais { get; set; }
        public ICollection<HopDongVanTaiLe> HopDongVanTaiLes { get; set; }


    }

}
