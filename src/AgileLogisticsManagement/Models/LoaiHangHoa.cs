﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    [Table(nameof(LoaiHangHoa))]
    public class LoaiHangHoa
    {
        public LoaiHangHoa()
        {
            this.HopDongVanTais = new HashSet<HopDongVanTai>();
            this.HopDongVanTaiLes = new HashSet<HopDongVanTaiLe>();
        }
        public int Id { get; set; }
        [Display(Name = "Tên")]
        public string Ten { get; set; }
        public ICollection<HopDongVanTai> HopDongVanTais { get; set; }
        public ICollection<HopDongVanTaiLe> HopDongVanTaiLes { get; set; }
    }

}
