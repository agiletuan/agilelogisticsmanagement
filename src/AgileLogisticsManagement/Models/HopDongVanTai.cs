﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{

    [Table(nameof(HopDongVanTai))]
    public class HopDongVanTai
    {
        
        public HopDongVanTai()
        {
            this.LoTrinhGiaos = new HashSet<LoTrinhGiao>();
            this.DonHangGheps = new HashSet<DonHangGhep>();
        }
        public int Id { get; set; }
        [Display(Name = "Mã hợp đồng")]
        public string MaHopDong { get; set; }
        /// <summary>
        /// Đối tác: ở đây thường là khách hàng.
        /// </summary>
        [Display(Name = "Đối tác", Description = "Đối tác: ở đây thường là khách hàng.")]
        public int IdDoiTac { get; set; }
        [Display(Name = "Đối tác", Description = "Đối tác: ở đây thường là khách hàng.")]
        public DoiTac DoiTac { get; set; }
        /// <summary>
        /// Tệp scan hợp đồng (dạng  pdf hoặc hình ảnh)
        /// </summary>
        [Display(Name = "Tệp hợp đồng", Description = "Là bản scan.")]
        public string FileHopDong { get; set; }
        /// <summary>
        /// Nội dung email của hợp đồng.
        /// Cho định dạng Html.
        /// </summary>
        [Display(Name = "Nội dung email", Description = "Nội dung của email từ đối tác gửi về cho đơn vị.")]
        public string NoiDungEmail { get; set; }
        [Display(Name = "Giá trị hợp đồng")]
        public double GiaTriHopDong { get; set; }

        /// <summary>
        /// Loại hàng hóa.
        /// </summary>
        [Display(Name = "Loại hàng hóa", Description = "Là loại hàng hóa mà hợp đồng này ký kết vận chuyển.")]
        public int IdLoaiHangHoa { get; set; }
        [Display(Name = "Loại hàng hóa", Description = "Là loại hàng hóa mà hợp đồng này ký kết vận chuyển.")]
        public LoaiHangHoa LoaiHangHoa { get; set; }
        /// <summary>
        /// Các đơn hàng ghép.
        /// Quan hệ n-n với HopDongVanTaiLe, nên phải tách ra thành n-1 & 1-n.
        /// </summary>
        [Display(Name = "Đơn hàng ghép", Description = "Các đơn hàng ghép với đơn hàng lẻ.")]
        public ICollection<DonHangGhep> DonHangGheps { get; set; }

        /// <summary>
        /// Các mục trên lộ trình giao hàng.
        /// </summary>
        [Display(Name = "Lộ trình giao", Description = "Danh sách các điểm trên lộ trình giao hàng chi tiết.")]
        public ICollection<LoTrinhGiao> LoTrinhGiaos { get; set; }

        [Display(Name = "Xe vận tải", Description = "Xe được chọn cho hợp đồng.")]
        public int IdXe { get; set; }
        [Display(Name = "Xe vận tải", Description = "Xe được chọn cho hợp đồng.")]
        public Xe Xe { get; set; }

        [Display(Name = "Tài xế", Description = "Tài xế được chọn cho hợp đồng.")]
        public int IdTaiXe { get; set; }
        [Display(Name = "Tài xế", Description = "Tài xế được chọn cho hợp đồng.")]
        public TaiXe TaiXe { get; set; }
        /// <summary>
        /// Thường là phí đường bộ.
        /// </summary>
        [Display(Name = "Lệ phí", Description = "Thường là lệ phí đường bộ của chuyến hàng")]
        public double LePhi { get; set; }
        /// <summary>
        /// Thuế.
        /// </summary>
        [Display(Name = "Thuế")]
        public double Thue { get; set; }

        /// <summary>
        /// Thường là nhân viên kinh doanh. Trưởng nhóm, nhân viên.
        /// </summary>
        [Display(Name = "Nhân viên")]
        public int IdNhanVien { get; set; }
        [Display(Name = "Nhân viên")]
        public NhanVien NhanVien { get; set; }

        [Display(Name = "Trạng thái", Description = "Trạng thái: Đang chuẩn bị, đang vận chuyển, đã giao")]
        public string TrangThaiHopDong { get; set; }

        public class LoaiTrangThaiHopDong
        {
            public const string DangChuanBi = "Đang chuẩn bị";
            public const string DangVanChuyen = "Đang vận chuyển";
            public const string DaGiao = "Đã giao";
        }
    }

}
