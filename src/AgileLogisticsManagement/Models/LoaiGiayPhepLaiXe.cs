﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    [Table(nameof(LoaiGiayPhepLaiXe))]
    public class LoaiGiayPhepLaiXe
    {
        public LoaiGiayPhepLaiXe()
        {
            this.TaiXes = new HashSet<TaiXe>();
        }
        public int Id { get; set; }
        [Display(Name = "Tên")]
        public string Ten { get; set; }
        public ICollection<TaiXe> TaiXes { get; set; }
    }

}
