﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgileLogisticsManagement.Models
{
    [Table(nameof(Xe))]
    public class Xe
    {
        public Xe()
        {
            this.HopDongVanTais = new HashSet<HopDongVanTai>();
        }
        public int Id { get; set; }
        [Display(Name = "Mã số")]
        public string MaSo { get; set; }
        [Display(Name = "Biển số")]
        public string BienSo { get; set; }

        [Display(Name = "Ngày kiểm định")]        
        public DateTime KiemDinh_Ngay { get; set; }

        [Display(Name = "Hạn kiểm định")]        
        public DateTime KiemDinh_Han { get; set; }

        /// <summary>
        /// Tài xế để tham khảo, trên hợp đồng, có thể là tài xế khác lái --> a Dũng xác nhận lúc 18h31p 15/02/2017
        /// Không set quan hệ với TaiXe để tránh quan hệ Cycle.
        /// </summary>
        [Display(Name = "Tài xế", Description = "Tùy chọn tài xế của xe vận tải")]
        public int IdTaiXe { get; set; }
        public ICollection<HopDongVanTai> HopDongVanTais { get; set; }
    }

}
