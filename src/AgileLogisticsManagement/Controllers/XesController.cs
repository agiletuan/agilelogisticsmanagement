using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    [Produces("application/json")]
    public class XesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public XesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Xes
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            //return View(await _context.Xes.ToListAsync());
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = _context.Xes.OrderByDescending(t => t.Id).Where(t => string.IsNullOrEmpty(t.BienSo) || t.BienSo.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: Xes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var xe = await _context.Xes.SingleOrDefaultAsync(m => m.Id == id);
            if (xe == null)
            {
                return NotFound();
            }

            return View(xe);
        }

        // GET: Xes/Create
        public IActionResult Create()
        {
            ViewData[nameof(Xe.IdTaiXe)] = new SelectList(_context.TaiXes, "Id", nameof(TaiXe.Ten));
            return View();
        }

        // POST: Xes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromBody][Bind("Id,BienSo,IdTaiXe,KiemDinh_Han,KiemDinh_Ngay,MaSo")] Xe xe)
        {
            if (ModelState.IsValid)
            {
                _context.Add(xe);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData[nameof(Xe.IdTaiXe)] = new SelectList(_context.TaiXes, "Id", nameof(TaiXe.Ten));
            return View(xe);
        }

        // GET: Xes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var xe = await _context.Xes.SingleOrDefaultAsync(m => m.Id == id);
            if (xe == null)
            {
                return NotFound();
            }
            ViewData[nameof(Xe.IdTaiXe)] = new SelectList(_context.TaiXes, "Id", nameof(TaiXe.Ten));
            return View(xe);
        }

        // POST: Xes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,BienSo,IdTaiXe,KiemDinh_Han,KiemDinh_Ngay,MaSo")] Xe xe)
        {
            if (id != xe.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(xe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!XeExists(xe.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData[nameof(Xe.IdTaiXe)] = new SelectList(_context.TaiXes, "Id", nameof(TaiXe.Ten));
            return View(xe);
        }

        // GET: Xes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var xe = await _context.Xes.SingleOrDefaultAsync(m => m.Id == id);
            if (xe == null)
            {
                return NotFound();
            }

            return View(xe);
        }

        // POST: Xes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var xe = await _context.Xes.SingleOrDefaultAsync(m => m.Id == id);
            _context.Xes.Remove(xe);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool XeExists(int id)
        {
            return _context.Xes.Any(e => e.Id == id);
        }
    }
}
