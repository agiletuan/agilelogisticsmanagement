using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class LoaiGiayPhepLaiXesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LoaiGiayPhepLaiXesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: LoaiGiayPhepLaiXes
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            //return View(await _context.LoaiGiayPhepLaiXes.ToListAsync());
        keyword = (keyword == null ? "" : keyword);
        rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
        var data = _context.LoaiGiayPhepLaiXes.OrderByDescending(t=>t.Id).Where(t => string.IsNullOrEmpty(t.Ten) || t.Ten.ToLower().Contains(keyword.ToLower()));
        var count = data.Count();
        var totalPage = count / rowPerPage;
        var mod = count % rowPerPage;
        totalPage += (mod > 0 ? 1 : 0);
        page = ((page < 1 || page > totalPage) ? 1 : page);
        data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
        ViewBag.totalPage = totalPage;
        ViewBag.keyword = keyword;
        ViewBag.page = page;
        ViewBag.rowPerPage = rowPerPage;
        return View(await data.ToListAsync());
        }

        // GET: LoaiGiayPhepLaiXes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiGiayPhepLaiXe = await _context.LoaiGiayPhepLaiXes.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiGiayPhepLaiXe == null)
            {
                return NotFound();
            }

            return View(loaiGiayPhepLaiXe);
        }

        // GET: LoaiGiayPhepLaiXes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LoaiGiayPhepLaiXes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Ten")] LoaiGiayPhepLaiXe loaiGiayPhepLaiXe)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loaiGiayPhepLaiXe);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(loaiGiayPhepLaiXe);
        }

        // GET: LoaiGiayPhepLaiXes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiGiayPhepLaiXe = await _context.LoaiGiayPhepLaiXes.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiGiayPhepLaiXe == null)
            {
                return NotFound();
            }
            return View(loaiGiayPhepLaiXe);
        }

        // POST: LoaiGiayPhepLaiXes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Ten")] LoaiGiayPhepLaiXe loaiGiayPhepLaiXe)
        {
            if (id != loaiGiayPhepLaiXe.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loaiGiayPhepLaiXe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoaiGiayPhepLaiXeExists(loaiGiayPhepLaiXe.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(loaiGiayPhepLaiXe);
        }

        // GET: LoaiGiayPhepLaiXes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiGiayPhepLaiXe = await _context.LoaiGiayPhepLaiXes.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiGiayPhepLaiXe == null)
            {
                return NotFound();
            }

            return View(loaiGiayPhepLaiXe);
        }

        // POST: LoaiGiayPhepLaiXes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var loaiGiayPhepLaiXe = await _context.LoaiGiayPhepLaiXes.SingleOrDefaultAsync(m => m.Id == id);
            _context.LoaiGiayPhepLaiXes.Remove(loaiGiayPhepLaiXe);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool LoaiGiayPhepLaiXeExists(int id)
        {
            return _context.LoaiGiayPhepLaiXes.Any(e => e.Id == id);
        }
    }
}
