using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class NhanViensController : Controller
    {
        private readonly ApplicationDbContext _context;

        public NhanViensController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: NhanViens
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            var applicationDbContext = _context.NhanViens.Include(n => n.LoaiChucVu);
            //return View(await _context.applicationDbContext.ToListAsync());
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = applicationDbContext.OrderByDescending(t => t.Id).Where(t => string.IsNullOrEmpty(t.Ten) || t.Ten.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: NhanViens/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nhanVien = await _context.NhanViens.SingleOrDefaultAsync(m => m.Id == id);
            if (nhanVien == null)
            {
                return NotFound();
            }

            return View(nhanVien);
        }

        // GET: NhanViens/Create
        public IActionResult Create()
        {
            ViewData["IdLoaiChucVu"] = new SelectList(_context.LoaiChucVus, "Id", nameof(LoaiChucVu.Ten));
            return View();
        }

        // POST: NhanViens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DiaChi,IdLoaiChucVu,MaSo,SoDienThoai,Ten")] NhanVien nhanVien)
        {
            if (ModelState.IsValid)
            {
                _context.Add(nhanVien);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdLoaiChucVu"] = new SelectList(_context.LoaiChucVus, "Id", nameof(LoaiChucVu.Ten));
            return View(nhanVien);
        }

        // GET: NhanViens/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nhanVien = await _context.NhanViens.SingleOrDefaultAsync(m => m.Id == id);
            if (nhanVien == null)
            {
                return NotFound();
            }
            ViewData["IdLoaiChucVu"] = new SelectList(_context.LoaiChucVus, "Id", nameof(LoaiChucVu.Ten), nhanVien.IdLoaiChucVu);
            return View(nhanVien);
        }

        // POST: NhanViens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DiaChi,IdLoaiChucVu,MaSo,SoDienThoai,Ten")] NhanVien nhanVien)
        {
            if (id != nhanVien.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(nhanVien);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NhanVienExists(nhanVien.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdLoaiChucVu"] = new SelectList(_context.LoaiChucVus, "Id", nameof(LoaiChucVu.Ten), nhanVien.IdLoaiChucVu);
            return View(nhanVien);
        }

        // GET: NhanViens/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nhanVien = await _context.NhanViens.SingleOrDefaultAsync(m => m.Id == id);
            if (nhanVien == null)
            {
                return NotFound();
            }

            return View(nhanVien);
        }

        // POST: NhanViens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var nhanVien = await _context.NhanViens.SingleOrDefaultAsync(m => m.Id == id);
            _context.NhanViens.Remove(nhanVien);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool NhanVienExists(int id)
        {
            return _context.NhanViens.Any(e => e.Id == id);
        }
    }
}
