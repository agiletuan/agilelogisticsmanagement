using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;
using Microsoft.VisualStudio.Web.CodeGeneration.EntityFrameworkCore;

namespace AgileLogisticsManagement.Controllers
{
    public class LoaiChucVusController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LoaiChucVusController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: LoaiChucVus
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = _context.LoaiChucVus.OrderByDescending(t => t.Id).Where(t => string.IsNullOrEmpty(t.Ten) || t.Ten.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: LoaiChucVus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiChucVu = await _context.LoaiChucVus.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiChucVu == null)
            {
                return NotFound();
            }

            return View(loaiChucVu);
        }

        // GET: LoaiChucVus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LoaiChucVus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Ten")] LoaiChucVu loaiChucVu)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loaiChucVu);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(loaiChucVu);
        }

        // GET: LoaiChucVus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiChucVu = await _context.LoaiChucVus.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiChucVu == null)
            {
                return NotFound();
            }
            return View(loaiChucVu);
        }

        // POST: LoaiChucVus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Ten")] LoaiChucVu loaiChucVu)
        {
            if (id != loaiChucVu.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loaiChucVu);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoaiChucVuExists(loaiChucVu.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(loaiChucVu);
        }

        // GET: LoaiChucVus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiChucVu = await _context.LoaiChucVus.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiChucVu == null)
            {
                return NotFound();
            }

            return View(loaiChucVu);
        }

        // POST: LoaiChucVus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var loaiChucVu = await _context.LoaiChucVus.SingleOrDefaultAsync(m => m.Id == id);
            _context.LoaiChucVus.Remove(loaiChucVu);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool LoaiChucVuExists(int id)
        {
            return _context.LoaiChucVus.Any(e => e.Id == id);
        }
    }
}
