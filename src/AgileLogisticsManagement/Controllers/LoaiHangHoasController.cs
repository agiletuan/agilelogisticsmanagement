using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class LoaiHangHoasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LoaiHangHoasController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: LoaiHangHoas
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            //return View(await _context.LoaiHangHoas.ToListAsync());
        keyword = (keyword == null ? "" : keyword);
        rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
        var data = _context.LoaiHangHoas.OrderByDescending(t=>t.Id).Where(t => string.IsNullOrEmpty(t.Ten) || t.Ten.ToLower().Contains(keyword.ToLower()));
        var count = data.Count();
        var totalPage = count / rowPerPage;
        var mod = count % rowPerPage;
        totalPage += (mod > 0 ? 1 : 0);
        page = ((page < 1 || page > totalPage) ? 1 : page);
        data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
        ViewBag.totalPage = totalPage;
        ViewBag.keyword = keyword;
        ViewBag.page = page;
        ViewBag.rowPerPage = rowPerPage;
        return View(await data.ToListAsync());
        }

        // GET: LoaiHangHoas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiHangHoa = await _context.LoaiHangHoas.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiHangHoa == null)
            {
                return NotFound();
            }

            return View(loaiHangHoa);
        }

        // GET: LoaiHangHoas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LoaiHangHoas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Ten")] LoaiHangHoa loaiHangHoa)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loaiHangHoa);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(loaiHangHoa);
        }

        // GET: LoaiHangHoas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiHangHoa = await _context.LoaiHangHoas.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiHangHoa == null)
            {
                return NotFound();
            }
            return View(loaiHangHoa);
        }

        // POST: LoaiHangHoas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Ten")] LoaiHangHoa loaiHangHoa)
        {
            if (id != loaiHangHoa.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loaiHangHoa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoaiHangHoaExists(loaiHangHoa.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(loaiHangHoa);
        }

        // GET: LoaiHangHoas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiHangHoa = await _context.LoaiHangHoas.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiHangHoa == null)
            {
                return NotFound();
            }

            return View(loaiHangHoa);
        }

        // POST: LoaiHangHoas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var loaiHangHoa = await _context.LoaiHangHoas.SingleOrDefaultAsync(m => m.Id == id);
            _context.LoaiHangHoas.Remove(loaiHangHoa);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool LoaiHangHoaExists(int id)
        {
            return _context.LoaiHangHoas.Any(e => e.Id == id);
        }
    }
}
