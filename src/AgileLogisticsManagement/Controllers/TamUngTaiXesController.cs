using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class TamUngTaiXesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TamUngTaiXesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TamUngTaiXes
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            var applicationDbContext = _context.TamUngTaiXes.Include(t => t.TaiXe);
            //return View(await _context.applicationDbContext.ToListAsync());
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = applicationDbContext.OrderByDescending(t => t.Id).Where(t => string.IsNullOrEmpty(t.TaiXe.Ten) || t.TaiXe.Ten.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: TamUngTaiXes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tamUngTaiXe = await _context.TamUngTaiXes.SingleOrDefaultAsync(m => m.Id == id);
            if (tamUngTaiXe == null)
            {
                return NotFound();
            }

            return View(tamUngTaiXe);
        }

        // GET: TamUngTaiXes/Create
        public IActionResult Create()
        {
            ViewData["IdTaiXe"] = new SelectList(_context.TaiXes, "Id", nameof(TaiXe.Ten));
            return View();
        }

        // POST: TamUngTaiXes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdHopDongVanTai,IdTaiXe,SoTien,ThoiGian")] TamUngTaiXe tamUngTaiXe)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tamUngTaiXe);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdTaiXe"] = new SelectList(_context.TaiXes, "Id", nameof(TaiXe.Ten), tamUngTaiXe.IdTaiXe);
            return View(tamUngTaiXe);
        }

        // GET: TamUngTaiXes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tamUngTaiXe = await _context.TamUngTaiXes.SingleOrDefaultAsync(m => m.Id == id);
            if (tamUngTaiXe == null)
            {
                return NotFound();
            }
            ViewData["IdTaiXe"] = new SelectList(_context.TaiXes, "Id", nameof(TaiXe.Ten), tamUngTaiXe.IdTaiXe);
            return View(tamUngTaiXe);
        }

        // POST: TamUngTaiXes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdHopDongVanTai,IdTaiXe,SoTien,ThoiGian")] TamUngTaiXe tamUngTaiXe)
        {
            if (id != tamUngTaiXe.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tamUngTaiXe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TamUngTaiXeExists(tamUngTaiXe.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdTaiXe"] = new SelectList(_context.TaiXes, "Id", nameof(TaiXe.Ten), tamUngTaiXe.IdTaiXe);
            return View(tamUngTaiXe);
        }

        // GET: TamUngTaiXes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tamUngTaiXe = await _context.TamUngTaiXes.SingleOrDefaultAsync(m => m.Id == id);
            if (tamUngTaiXe == null)
            {
                return NotFound();
            }

            return View(tamUngTaiXe);
        }

        // POST: TamUngTaiXes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tamUngTaiXe = await _context.TamUngTaiXes.SingleOrDefaultAsync(m => m.Id == id);
            _context.TamUngTaiXes.Remove(tamUngTaiXe);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TamUngTaiXeExists(int id)
        {
            return _context.TamUngTaiXes.Any(e => e.Id == id);
        }
    }
}
