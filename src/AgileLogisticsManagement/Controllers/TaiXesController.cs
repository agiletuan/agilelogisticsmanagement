using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class TaiXesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TaiXesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TaiXes
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            var applicationDbContext = _context.TaiXes.Include(t => t.LoaiGiayPhepLaiXe);
            //return View(await _context.applicationDbContext.ToListAsync());
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = applicationDbContext.OrderByDescending(t => t.Id).Where(t => string.IsNullOrEmpty(t.Ten) || t.Ten.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: TaiXes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taiXe = await _context.TaiXes.SingleOrDefaultAsync(m => m.Id == id);
            if (taiXe == null)
            {
                return NotFound();
            }

            return View(taiXe);
        }

        // GET: TaiXes/Create
        public IActionResult Create()
        {
            ViewData["IdLoaiGiayPhepLaiXe"] = new SelectList(_context.LoaiGiayPhepLaiXes, "Id", nameof(LoaiGiayPhepLaiXe.Ten));
            return View();
        }

        // POST: TaiXes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DiaChi,IdLoaiGiayPhepLaiXe,MaSo,SoDienThoai,Ten")] TaiXe taiXe)
        {
            if (ModelState.IsValid)
            {
                _context.Add(taiXe);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdLoaiGiayPhepLaiXe"] = new SelectList(_context.LoaiGiayPhepLaiXes, "Id", nameof(LoaiGiayPhepLaiXe.Ten), taiXe.IdLoaiGiayPhepLaiXe);
            return View(taiXe);
        }

        // GET: TaiXes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taiXe = await _context.TaiXes.SingleOrDefaultAsync(m => m.Id == id);
            if (taiXe == null)
            {
                return NotFound();
            }
            ViewData["IdLoaiGiayPhepLaiXe"] = new SelectList(_context.LoaiGiayPhepLaiXes, "Id", nameof(LoaiGiayPhepLaiXe.Ten), taiXe.IdLoaiGiayPhepLaiXe);
            return View(taiXe);
        }

        // POST: TaiXes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DiaChi,IdLoaiGiayPhepLaiXe,MaSo,SoDienThoai,Ten")] TaiXe taiXe)
        {
            if (id != taiXe.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(taiXe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TaiXeExists(taiXe.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdLoaiGiayPhepLaiXe"] = new SelectList(_context.LoaiGiayPhepLaiXes, "Id", nameof(LoaiGiayPhepLaiXe.Ten), taiXe.IdLoaiGiayPhepLaiXe);
            return View(taiXe);
        }

        // GET: TaiXes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taiXe = await _context.TaiXes.SingleOrDefaultAsync(m => m.Id == id);
            if (taiXe == null)
            {
                return NotFound();
            }

            return View(taiXe);
        }

        // POST: TaiXes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var taiXe = await _context.TaiXes.SingleOrDefaultAsync(m => m.Id == id);
            _context.TaiXes.Remove(taiXe);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TaiXeExists(int id)
        {
            return _context.TaiXes.Any(e => e.Id == id);
        }
    }
}
