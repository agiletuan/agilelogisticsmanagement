using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/LoaiChucVusApi")]
    public class LoaiChucVusApiController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LoaiChucVusApiController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/LoaiChucVusApi
        [HttpGet]
        public IEnumerable<LoaiChucVu> GetLoaiChucVus()
        {
            return _context.LoaiChucVus;
        }

        // GET: api/LoaiChucVusApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLoaiChucVu([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LoaiChucVu loaiChucVu = await _context.LoaiChucVus.SingleOrDefaultAsync(m => m.Id == id);

            if (loaiChucVu == null)
            {
                return NotFound();
            }

            return Ok(loaiChucVu);
        }

        // PUT: api/LoaiChucVusApi/5
        [HttpPost][Route("Edit")]
        public async Task<IActionResult> PutLoaiChucVu(int id, [FromBody] LoaiChucVu loaiChucVu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != loaiChucVu.Id)
            {
                return BadRequest();
            }

            _context.Entry(loaiChucVu).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoaiChucVuExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LoaiChucVusApi
        [HttpPost][Route("Create")]
        public async Task<IActionResult> PostLoaiChucVu([FromBody] LoaiChucVu loaiChucVu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.LoaiChucVus.Add(loaiChucVu);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LoaiChucVuExists(loaiChucVu.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetLoaiChucVu", new { id = loaiChucVu.Id }, loaiChucVu);
        }

        // DELETE: api/LoaiChucVusApi/5
        [HttpPost][Route("Delete")]
        public async Task<IActionResult> DeleteLoaiChucVu(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LoaiChucVu loaiChucVu = await _context.LoaiChucVus.SingleOrDefaultAsync(m => m.Id == id);
            if (loaiChucVu == null)
            {
                return NotFound();
            }

            _context.LoaiChucVus.Remove(loaiChucVu);
            await _context.SaveChangesAsync();

            return Ok(loaiChucVu);
        }

        private bool LoaiChucVuExists(int id)
        {
            return _context.LoaiChucVus.Any(e => e.Id == id);
        }
    }
}