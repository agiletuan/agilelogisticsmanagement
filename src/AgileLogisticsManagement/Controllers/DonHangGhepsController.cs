using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class DonHangGhepsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DonHangGhepsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: DonHangGheps
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            var applicationDbContext = _context.DonHangGheps.Include(d => d.HopDongVanTai).Include(d => d.HopDongVanTaiLe);
            //return View(await _context.applicationDbContext.ToListAsync());
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = applicationDbContext.OrderByDescending(t => t.Id).Where(t => string.IsNullOrEmpty(t.HopDongVanTai.MaHopDong) || t.HopDongVanTai.MaHopDong.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: DonHangGheps/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donHangGhep = await _context.DonHangGheps.SingleOrDefaultAsync(m => m.Id == id);
            if (donHangGhep == null)
            {
                return NotFound();
            }

            return View(donHangGhep);
        }

        // GET: DonHangGheps/Create
        public IActionResult Create()
        {
            ViewData["IdHopDongVanTai"] = new SelectList(_context.HopDongVanTais, "Id", "Id");
            ViewData["IdHopDongVanTaiLe"] = new SelectList(_context.HopDongVanTaiLes, "Id", "Id");
            return View();
        }

        // POST: DonHangGheps/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdHopDongVanTai,IdHopDongVanTaiLe")] DonHangGhep donHangGhep)
        {
            if (ModelState.IsValid)
            {
                _context.Add(donHangGhep);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdHopDongVanTai"] = new SelectList(_context.HopDongVanTais, "Id", "Id", donHangGhep.IdHopDongVanTai);
            ViewData["IdHopDongVanTaiLe"] = new SelectList(_context.HopDongVanTaiLes, "Id", "Id", donHangGhep.IdHopDongVanTaiLe);
            return View(donHangGhep);
        }

        // GET: DonHangGheps/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donHangGhep = await _context.DonHangGheps.SingleOrDefaultAsync(m => m.Id == id);
            if (donHangGhep == null)
            {
                return NotFound();
            }
            ViewData["IdHopDongVanTai"] = new SelectList(_context.HopDongVanTais, "Id", "Id", donHangGhep.IdHopDongVanTai);
            ViewData["IdHopDongVanTaiLe"] = new SelectList(_context.HopDongVanTaiLes, "Id", "Id", donHangGhep.IdHopDongVanTaiLe);
            return View(donHangGhep);
        }

        // POST: DonHangGheps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdHopDongVanTai,IdHopDongVanTaiLe")] DonHangGhep donHangGhep)
        {
            if (id != donHangGhep.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(donHangGhep);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DonHangGhepExists(donHangGhep.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdHopDongVanTai"] = new SelectList(_context.HopDongVanTais, "Id", "Id", donHangGhep.IdHopDongVanTai);
            ViewData["IdHopDongVanTaiLe"] = new SelectList(_context.HopDongVanTaiLes, "Id", "Id", donHangGhep.IdHopDongVanTaiLe);
            return View(donHangGhep);
        }

        // GET: DonHangGheps/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donHangGhep = await _context.DonHangGheps.SingleOrDefaultAsync(m => m.Id == id);
            if (donHangGhep == null)
            {
                return NotFound();
            }

            return View(donHangGhep);
        }

        // POST: DonHangGheps/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var donHangGhep = await _context.DonHangGheps.SingleOrDefaultAsync(m => m.Id == id);
            _context.DonHangGheps.Remove(donHangGhep);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool DonHangGhepExists(int id)
        {
            return _context.DonHangGheps.Any(e => e.Id == id);
        }
    }
}
