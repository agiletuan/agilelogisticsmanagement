using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class HopDongVanTaiLesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HopDongVanTaiLesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HopDongVanTaiLes
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            var applicationDbContext = _context.HopDongVanTaiLes.Include(h => h.DoiTac).Include(h => h.LoaiHangHoa);
            //return View(await _context.applicationDbContext.ToListAsync());
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = applicationDbContext.OrderByDescending(t => t.Id).Where(t => string.IsNullOrEmpty(t.DoiTac.Ten) || t.DoiTac.Ten.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: HopDongVanTaiLes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hopDongVanTaiLe = await _context.HopDongVanTaiLes.SingleOrDefaultAsync(m => m.Id == id);
            if (hopDongVanTaiLe == null)
            {
                return NotFound();
            }

            return View(hopDongVanTaiLe);
        }

        // GET: HopDongVanTaiLes/Create
        public IActionResult Create()
        {
            ViewData["IdDoiTac"] = new SelectList(_context.DoiTacs, "Id", "Id");
            ViewData["IdLoaiHangHoa"] = new SelectList(_context.LoaiHangHoas, "Id", "Id");
            return View();
        }

        // POST: HopDongVanTaiLes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdDoiTac,IdLoaiHangHoa")] HopDongVanTaiLe hopDongVanTaiLe)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hopDongVanTaiLe);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdDoiTac"] = new SelectList(_context.DoiTacs, "Id", "Id", hopDongVanTaiLe.IdDoiTac);
            ViewData["IdLoaiHangHoa"] = new SelectList(_context.LoaiHangHoas, "Id", "Id", hopDongVanTaiLe.IdLoaiHangHoa);
            return View(hopDongVanTaiLe);
        }

        // GET: HopDongVanTaiLes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hopDongVanTaiLe = await _context.HopDongVanTaiLes.SingleOrDefaultAsync(m => m.Id == id);
            if (hopDongVanTaiLe == null)
            {
                return NotFound();
            }
            ViewData["IdDoiTac"] = new SelectList(_context.DoiTacs, "Id", "Id", hopDongVanTaiLe.IdDoiTac);
            ViewData["IdLoaiHangHoa"] = new SelectList(_context.LoaiHangHoas, "Id", "Id", hopDongVanTaiLe.IdLoaiHangHoa);
            return View(hopDongVanTaiLe);
        }

        // POST: HopDongVanTaiLes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdDoiTac,IdLoaiHangHoa")] HopDongVanTaiLe hopDongVanTaiLe)
        {
            if (id != hopDongVanTaiLe.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hopDongVanTaiLe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HopDongVanTaiLeExists(hopDongVanTaiLe.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdDoiTac"] = new SelectList(_context.DoiTacs, "Id", "Id", hopDongVanTaiLe.IdDoiTac);
            ViewData["IdLoaiHangHoa"] = new SelectList(_context.LoaiHangHoas, "Id", "Id", hopDongVanTaiLe.IdLoaiHangHoa);
            return View(hopDongVanTaiLe);
        }

        // GET: HopDongVanTaiLes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hopDongVanTaiLe = await _context.HopDongVanTaiLes.SingleOrDefaultAsync(m => m.Id == id);
            if (hopDongVanTaiLe == null)
            {
                return NotFound();
            }

            return View(hopDongVanTaiLe);
        }

        // POST: HopDongVanTaiLes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hopDongVanTaiLe = await _context.HopDongVanTaiLes.SingleOrDefaultAsync(m => m.Id == id);
            _context.HopDongVanTaiLes.Remove(hopDongVanTaiLe);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool HopDongVanTaiLeExists(int id)
        {
            return _context.HopDongVanTaiLes.Any(e => e.Id == id);
        }
    }
}
