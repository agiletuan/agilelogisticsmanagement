using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class DoiTacsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DoiTacsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: DoiTacs
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            var applicationDbContext = _context.DoiTacs.Include(d => d.LinhVucKinhDoanh);
            //return View(await _context.applicationDbContext.ToListAsync());
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = applicationDbContext.OrderByDescending(t => t.Id).Where(t => string.IsNullOrEmpty(t.Ten) || t.Ten.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: DoiTacs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var doiTac = await _context.DoiTacs.SingleOrDefaultAsync(m => m.Id == id);
            if (doiTac == null)
            {
                return NotFound();
            }

            return View(doiTac);
        }

        // GET: DoiTacs/Create
        public IActionResult Create()
        {
            ViewData["IdLinhVucKinhDoanh"] = new SelectList(_context.LinhVucKinhDoanhs, "Id", nameof(LinhVucKinhDoanh.Ten));
            return View();
        }

        // POST: DoiTacs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CMND,DiaChi,IdLinhVucKinhDoanh,IsCaNhan,LoaiHangHoaVanTai,MaSo,MaSoThue,NguoiLienHe,SoDienThoai,Ten,ThongTinXuatHoaDon")] DoiTac doiTac)
        {
            if (ModelState.IsValid)
            {
                _context.Add(doiTac);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdLinhVucKinhDoanh"] = new SelectList(_context.LinhVucKinhDoanhs, "Id", nameof(LinhVucKinhDoanh.Ten), doiTac.IdLinhVucKinhDoanh);
            return View(doiTac);
        }

        // GET: DoiTacs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var doiTac = await _context.DoiTacs.SingleOrDefaultAsync(m => m.Id == id);
            if (doiTac == null)
            {
                return NotFound();
            }
            ViewData["IdLinhVucKinhDoanh"] = new SelectList(_context.LinhVucKinhDoanhs, "Id", nameof(LinhVucKinhDoanh.Ten), doiTac.IdLinhVucKinhDoanh);
            return View(doiTac);
        }

        // POST: DoiTacs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CMND,DiaChi,IdLinhVucKinhDoanh,IsCaNhan,LoaiHangHoaVanTai,MaSo,MaSoThue,NguoiLienHe,SoDienThoai,Ten,ThongTinXuatHoaDon")] DoiTac doiTac)
        {
            if (id != doiTac.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(doiTac);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DoiTacExists(doiTac.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdLinhVucKinhDoanh"] = new SelectList(_context.LinhVucKinhDoanhs, "Id", nameof(LinhVucKinhDoanh.Ten), doiTac.IdLinhVucKinhDoanh);
            return View(doiTac);
        }

        // GET: DoiTacs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var doiTac = await _context.DoiTacs.SingleOrDefaultAsync(m => m.Id == id);
            if (doiTac == null)
            {
                return NotFound();
            }

            return View(doiTac);
        }

        // POST: DoiTacs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var doiTac = await _context.DoiTacs.SingleOrDefaultAsync(m => m.Id == id);
            _context.DoiTacs.Remove(doiTac);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool DoiTacExists(int id)
        {
            return _context.DoiTacs.Any(e => e.Id == id);
        }
    }
}
