using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class HopDongVanTaisController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HopDongVanTaisController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HopDongVanTais
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            var applicationDbContext = _context.HopDongVanTais
                .Include(h => h.LoaiHangHoa).Include(h => h.NhanVien).Include(h => h.TaiXe).Include(h => h.Xe);
            //return View(await _context.applicationDbContext.ToListAsync());
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = applicationDbContext.OrderByDescending(t => t.Id)
                .Where(t => string.IsNullOrEmpty(t.MaHopDong) || t.MaHopDong.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: HopDongVanTais/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hopDongVanTai = await _context.HopDongVanTais.SingleOrDefaultAsync(m => m.Id == id);
            if (hopDongVanTai == null)
            {
                return NotFound();
            }

            return View(hopDongVanTai);
        }

        // GET: HopDongVanTais/Create
        public IActionResult Create()
        {
            ViewData["IdLoaiHangHoa"] = new SelectList(_context.LoaiHangHoas, "Id", "Ten");
            ViewData["IdNhanVien"] = new SelectList(_context.NhanViens, "Id", "Ten");
            ViewData["IdTaiXe"] = new SelectList(_context.TaiXes, "Id", "Ten");
            ViewData["IdXe"] = new SelectList(_context.Xes, "Id", nameof(Xe.BienSo));
            return View();
        }

        // POST: HopDongVanTais/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FileHopDong,GiaTriHopDong,IdDoiTac,IdLoaiHangHoa,IdNhanVien,IdTaiXe,IdXe,LePhi,MaHopDong,NoiDungEmail,Thue,TrangThaiHopDong")] HopDongVanTai hopDongVanTai)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hopDongVanTai);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdLoaiHangHoa"] = new SelectList(_context.LoaiHangHoas, "Id", "Ten", hopDongVanTai.IdLoaiHangHoa);
            ViewData["IdNhanVien"] = new SelectList(_context.NhanViens, "Id", "Ten", hopDongVanTai.IdNhanVien);
            ViewData["IdTaiXe"] = new SelectList(_context.TaiXes, "Id", "Ten", hopDongVanTai.IdTaiXe);
            ViewData["IdXe"] = new SelectList(_context.Xes, "Id", "BienSo", hopDongVanTai.IdXe);
            return View(hopDongVanTai);
        }

        // GET: HopDongVanTais/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hopDongVanTai = await _context.HopDongVanTais.SingleOrDefaultAsync(m => m.Id == id);
            if (hopDongVanTai == null)
            {
                return NotFound();
            }
            ViewData["IdLoaiHangHoa"] = new SelectList(_context.LoaiHangHoas, "Id", "Ten", hopDongVanTai.IdLoaiHangHoa);
            ViewData["IdNhanVien"] = new SelectList(_context.NhanViens, "Id", "Ten", hopDongVanTai.IdNhanVien);
            ViewData["IdTaiXe"] = new SelectList(_context.TaiXes, "Id", "Ten", hopDongVanTai.IdTaiXe);
            ViewData["IdXe"] = new SelectList(_context.Xes, "Id", "BienSo", hopDongVanTai.IdXe);
            return View(hopDongVanTai);
        }

        // POST: HopDongVanTais/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FileHopDong,GiaTriHopDong,IdDoiTac,IdLoaiHangHoa,IdNhanVien,IdTaiXe,IdXe,LePhi,MaHopDong,NoiDungEmail,Thue,TrangThaiHopDong")] HopDongVanTai hopDongVanTai)
        {
            if (id != hopDongVanTai.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hopDongVanTai);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HopDongVanTaiExists(hopDongVanTai.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdLoaiHangHoa"] = new SelectList(_context.LoaiHangHoas, "Id", "Ten", hopDongVanTai.IdLoaiHangHoa);
            ViewData["IdNhanVien"] = new SelectList(_context.NhanViens, "Id", "Ten", hopDongVanTai.IdNhanVien);
            ViewData["IdTaiXe"] = new SelectList(_context.TaiXes, "Id", "Ten", hopDongVanTai.IdTaiXe);
            ViewData["IdXe"] = new SelectList(_context.Xes, "Id", "BienSo", hopDongVanTai.IdXe);
            return View(hopDongVanTai);
        }

        // GET: HopDongVanTais/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hopDongVanTai = await _context.HopDongVanTais.SingleOrDefaultAsync(m => m.Id == id);
            if (hopDongVanTai == null)
            {
                return NotFound();
            }

            return View(hopDongVanTai);
        }

        // POST: HopDongVanTais/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hopDongVanTai = await _context.HopDongVanTais.SingleOrDefaultAsync(m => m.Id == id);
            _context.HopDongVanTais.Remove(hopDongVanTai);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool HopDongVanTaiExists(int id)
        {
            return _context.HopDongVanTais.Any(e => e.Id == id);
        }
    }
}
