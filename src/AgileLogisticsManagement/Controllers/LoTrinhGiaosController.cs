using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    public class LoTrinhGiaosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LoTrinhGiaosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: LoTrinhGiaos
        public async Task<IActionResult> Index(int page, int rowPerPage, string keyword)
        {
            var applicationDbContext = _context.LoTrinhGiaos.Include(l => l.HopDongVanTai);
            //return View(await _context.applicationDbContext.ToListAsync());
            keyword = (keyword == null ? "" : keyword);
            rowPerPage = (rowPerPage < 1 ? 10 : rowPerPage);
            var data = applicationDbContext.OrderByDescending(t => t.Id).Where(t => string.IsNullOrEmpty(t.HopDongVanTai.MaHopDong) || t.HopDongVanTai.MaHopDong.ToLower().Contains(keyword.ToLower()));
            var count = data.Count();
            var totalPage = count / rowPerPage;
            var mod = count % rowPerPage;
            totalPage += (mod > 0 ? 1 : 0);
            page = ((page < 1 || page > totalPage) ? 1 : page);
            data = data.Skip((page - 1) * rowPerPage).Take(rowPerPage);
            ViewBag.totalPage = totalPage;
            ViewBag.keyword = keyword;
            ViewBag.page = page;
            ViewBag.rowPerPage = rowPerPage;
            return View(await data.ToListAsync());
        }

        // GET: LoTrinhGiaos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loTrinhGiao = await _context.LoTrinhGiaos.SingleOrDefaultAsync(m => m.Id == id);
            if (loTrinhGiao == null)
            {
                return NotFound();
            }

            return View(loTrinhGiao);
        }

        // GET: LoTrinhGiaos/Create
        public IActionResult Create()
        {
            ViewData["IdHopDongVanTai"] = new SelectList(_context.HopDongVanTais, "Id", "Id");
            return View();
        }

        // POST: LoTrinhGiaos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DiaDiem,IdHopDongVanTai,NguoiLienHe,NguoiNhan,SoDienThoai,ThoiGian")] LoTrinhGiao loTrinhGiao)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loTrinhGiao);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdHopDongVanTai"] = new SelectList(_context.HopDongVanTais, "Id", "Id", loTrinhGiao.IdHopDongVanTai);
            return View(loTrinhGiao);
        }

        // GET: LoTrinhGiaos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loTrinhGiao = await _context.LoTrinhGiaos.SingleOrDefaultAsync(m => m.Id == id);
            if (loTrinhGiao == null)
            {
                return NotFound();
            }
            ViewData["IdHopDongVanTai"] = new SelectList(_context.HopDongVanTais, "Id", "Id", loTrinhGiao.IdHopDongVanTai);
            return View(loTrinhGiao);
        }

        // POST: LoTrinhGiaos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DiaDiem,IdHopDongVanTai,NguoiLienHe,NguoiNhan,SoDienThoai,ThoiGian")] LoTrinhGiao loTrinhGiao)
        {
            if (id != loTrinhGiao.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loTrinhGiao);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoTrinhGiaoExists(loTrinhGiao.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdHopDongVanTai"] = new SelectList(_context.HopDongVanTais, "Id", "Id", loTrinhGiao.IdHopDongVanTai);
            return View(loTrinhGiao);
        }

        // GET: LoTrinhGiaos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loTrinhGiao = await _context.LoTrinhGiaos.SingleOrDefaultAsync(m => m.Id == id);
            if (loTrinhGiao == null)
            {
                return NotFound();
            }

            return View(loTrinhGiao);
        }

        // POST: LoTrinhGiaos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var loTrinhGiao = await _context.LoTrinhGiaos.SingleOrDefaultAsync(m => m.Id == id);
            _context.LoTrinhGiaos.Remove(loTrinhGiao);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool LoTrinhGiaoExists(int id)
        {
            return _context.LoTrinhGiaos.Any(e => e.Id == id);
        }
    }
}
