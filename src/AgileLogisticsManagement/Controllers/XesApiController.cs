using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Data;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/XesApi")]
    public class XesApiController : Controller
    {
        private readonly ApplicationDbContext _context;

        public XesApiController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/XesApi
        [HttpGet]
        public IEnumerable<Xe> GetXes()
        {
            return _context.Xes;
        }

        // GET: api/XesApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetXe([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Xe xe = await _context.Xes.SingleOrDefaultAsync(m => m.Id == id);

            if (xe == null)
            {
                return NotFound();
            }

            return Ok(xe);
        }

        // PUT: api/XesApi/5
        [HttpPost][Route("Edit")]
        public async Task<IActionResult> PutXe(int id, [FromBody] Xe xe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != xe.Id)
            {
                return BadRequest();
            }

            _context.Entry(xe).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!XeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/XesApi
        [HttpPost][Route("Create")]
        public async Task<IActionResult> PostXe([FromBody] Xe xe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Xes.Add(xe);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (XeExists(xe.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetXe", new { id = xe.Id }, xe);
        }

        // DELETE: api/XesApi/5
        [HttpPost][Route("Delete")]
        public async Task<IActionResult> DeleteXe(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Xe xe = await _context.Xes.SingleOrDefaultAsync(m => m.Id == id);
            if (xe == null)
            {
                return NotFound();
            }

            _context.Xes.Remove(xe);
            await _context.SaveChangesAsync();

            return Ok(xe);
        }

        private bool XeExists(int id)
        {
            return _context.Xes.Any(e => e.Id == id);
        }
    }
}