﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AgileLogisticsManagement.Data.Migrations
{
    public partial class initAppModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LinhVucKinhDoanh",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ten = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinhVucKinhDoanh", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LoaiChucVu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ten = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiChucVu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LoaiGiayPhepLaiXe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ten = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiGiayPhepLaiXe", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LoaiHangHoa",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ten = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiHangHoa", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Xe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BienSo = table.Column<string>(nullable: true),
                    IdTaiXe = table.Column<int>(nullable: false),
                    KiemDinh_Han = table.Column<DateTime>(nullable: false),
                    KiemDinh_Ngay = table.Column<DateTime>(nullable: false),
                    MaSo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Xe", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DoiTac",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CMND = table.Column<string>(nullable: true),
                    DiaChi = table.Column<string>(nullable: true),
                    IdLinhVucKinhDoanh = table.Column<int>(nullable: false),
                    IsCaNhan = table.Column<bool>(nullable: false),
                    LinhVucKinhDoanhId = table.Column<int>(nullable: true),
                    LoaiHangHoaVanTai = table.Column<string>(nullable: true),
                    MaSo = table.Column<string>(nullable: true),
                    MaSoThue = table.Column<string>(nullable: true),
                    NguoiLienHe = table.Column<string>(nullable: true),
                    SoDienThoai = table.Column<string>(nullable: true),
                    Ten = table.Column<string>(nullable: true),
                    ThongTinXuatHoaDon = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoiTac", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DoiTac_LinhVucKinhDoanh_LinhVucKinhDoanhId",
                        column: x => x.LinhVucKinhDoanhId,
                        principalTable: "LinhVucKinhDoanh",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NhanVien",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DiaChi = table.Column<string>(nullable: true),
                    IdLoaiChucVu = table.Column<int>(nullable: false),
                    LoaiChucVuId = table.Column<int>(nullable: true),
                    MaSo = table.Column<string>(nullable: true),
                    SoDienThoai = table.Column<string>(nullable: true),
                    Ten = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhanVien", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NhanVien_LoaiChucVu_LoaiChucVuId",
                        column: x => x.LoaiChucVuId,
                        principalTable: "LoaiChucVu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TaiXe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DiaChi = table.Column<string>(nullable: true),
                    IdLoaiGiayPhepLaiXe = table.Column<int>(nullable: false),
                    LoaiGiayPhepLaiXeId = table.Column<int>(nullable: true),
                    MaSo = table.Column<string>(nullable: true),
                    SoDienThoai = table.Column<string>(nullable: true),
                    Ten = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaiXe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaiXe_LoaiGiayPhepLaiXe_LoaiGiayPhepLaiXeId",
                        column: x => x.LoaiGiayPhepLaiXeId,
                        principalTable: "LoaiGiayPhepLaiXe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HopDongVanTaiLe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DoiTacId = table.Column<int>(nullable: true),
                    IdDoiTac = table.Column<int>(nullable: false),
                    IdLoaiHangHoa = table.Column<int>(nullable: false),
                    LoaiHangHoaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HopDongVanTaiLe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HopDongVanTaiLe_DoiTac_DoiTacId",
                        column: x => x.DoiTacId,
                        principalTable: "DoiTac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HopDongVanTaiLe_LoaiHangHoa_LoaiHangHoaId",
                        column: x => x.LoaiHangHoaId,
                        principalTable: "LoaiHangHoa",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HopDongVanTai",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DoiTacId = table.Column<int>(nullable: true),
                    FileHopDong = table.Column<string>(nullable: true),
                    GiaTriHopDong = table.Column<double>(nullable: false),
                    IdDoiTac = table.Column<int>(nullable: false),
                    IdLoaiHangHoa = table.Column<int>(nullable: false),
                    IdNhanVien = table.Column<int>(nullable: false),
                    IdTaiXe = table.Column<int>(nullable: false),
                    LePhi = table.Column<double>(nullable: false),
                    LoaiHangHoaId = table.Column<int>(nullable: true),
                    MaHopDong = table.Column<string>(nullable: true),
                    NhanVienId = table.Column<int>(nullable: true),
                    NoiDungEmail = table.Column<string>(nullable: true),
                    TaiXeId = table.Column<int>(nullable: true),
                    Thue = table.Column<double>(nullable: false),
                    TrangThaiHopDong = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HopDongVanTai", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HopDongVanTai_DoiTac_DoiTacId",
                        column: x => x.DoiTacId,
                        principalTable: "DoiTac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HopDongVanTai_LoaiHangHoa_LoaiHangHoaId",
                        column: x => x.LoaiHangHoaId,
                        principalTable: "LoaiHangHoa",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HopDongVanTai_NhanVien_NhanVienId",
                        column: x => x.NhanVienId,
                        principalTable: "NhanVien",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HopDongVanTai_TaiXe_TaiXeId",
                        column: x => x.TaiXeId,
                        principalTable: "TaiXe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TamUngTaiXe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdHopDongVanTai = table.Column<int>(nullable: false),
                    IdTaiXe = table.Column<int>(nullable: false),
                    SoTien = table.Column<double>(nullable: false),
                    TaiXeId = table.Column<int>(nullable: true),
                    ThoiGian = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TamUngTaiXe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TamUngTaiXe_TaiXe_TaiXeId",
                        column: x => x.TaiXeId,
                        principalTable: "TaiXe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DonHangGhep",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HopDongVanTaiId = table.Column<int>(nullable: true),
                    HopDongVanTaiLeId = table.Column<int>(nullable: true),
                    IdHopDongVanTai = table.Column<int>(nullable: false),
                    IdHopDongVanTaiLe = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DonHangGhep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DonHangGhep_HopDongVanTai_HopDongVanTaiId",
                        column: x => x.HopDongVanTaiId,
                        principalTable: "HopDongVanTai",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DonHangGhep_HopDongVanTaiLe_HopDongVanTaiLeId",
                        column: x => x.HopDongVanTaiLeId,
                        principalTable: "HopDongVanTaiLe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LoTrinhGiao",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DiaDiem = table.Column<string>(nullable: true),
                    HopDongVanTaiId = table.Column<int>(nullable: true),
                    IdHopDongVanTai = table.Column<int>(nullable: false),
                    NguoiLienHe = table.Column<string>(nullable: true),
                    NguoiNhan = table.Column<string>(nullable: true),
                    SoDienThoai = table.Column<string>(nullable: true),
                    ThoiGian = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoTrinhGiao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LoTrinhGiao_HopDongVanTai_HopDongVanTaiId",
                        column: x => x.HopDongVanTaiId,
                        principalTable: "HopDongVanTai",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DoiTac_LinhVucKinhDoanhId",
                table: "DoiTac",
                column: "LinhVucKinhDoanhId");

            migrationBuilder.CreateIndex(
                name: "IX_DonHangGhep_HopDongVanTaiId",
                table: "DonHangGhep",
                column: "HopDongVanTaiId");

            migrationBuilder.CreateIndex(
                name: "IX_DonHangGhep_HopDongVanTaiLeId",
                table: "DonHangGhep",
                column: "HopDongVanTaiLeId");

            migrationBuilder.CreateIndex(
                name: "IX_HopDongVanTai_DoiTacId",
                table: "HopDongVanTai",
                column: "DoiTacId");

            migrationBuilder.CreateIndex(
                name: "IX_HopDongVanTai_LoaiHangHoaId",
                table: "HopDongVanTai",
                column: "LoaiHangHoaId");

            migrationBuilder.CreateIndex(
                name: "IX_HopDongVanTai_NhanVienId",
                table: "HopDongVanTai",
                column: "NhanVienId");

            migrationBuilder.CreateIndex(
                name: "IX_HopDongVanTai_TaiXeId",
                table: "HopDongVanTai",
                column: "TaiXeId");

            migrationBuilder.CreateIndex(
                name: "IX_HopDongVanTaiLe_DoiTacId",
                table: "HopDongVanTaiLe",
                column: "DoiTacId");

            migrationBuilder.CreateIndex(
                name: "IX_HopDongVanTaiLe_LoaiHangHoaId",
                table: "HopDongVanTaiLe",
                column: "LoaiHangHoaId");

            migrationBuilder.CreateIndex(
                name: "IX_LoTrinhGiao_HopDongVanTaiId",
                table: "LoTrinhGiao",
                column: "HopDongVanTaiId");

            migrationBuilder.CreateIndex(
                name: "IX_NhanVien_LoaiChucVuId",
                table: "NhanVien",
                column: "LoaiChucVuId");

            migrationBuilder.CreateIndex(
                name: "IX_TaiXe_LoaiGiayPhepLaiXeId",
                table: "TaiXe",
                column: "LoaiGiayPhepLaiXeId");

            migrationBuilder.CreateIndex(
                name: "IX_TamUngTaiXe_TaiXeId",
                table: "TamUngTaiXe",
                column: "TaiXeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DonHangGhep");

            migrationBuilder.DropTable(
                name: "LoTrinhGiao");

            migrationBuilder.DropTable(
                name: "TamUngTaiXe");

            migrationBuilder.DropTable(
                name: "Xe");

            migrationBuilder.DropTable(
                name: "HopDongVanTaiLe");

            migrationBuilder.DropTable(
                name: "HopDongVanTai");

            migrationBuilder.DropTable(
                name: "DoiTac");

            migrationBuilder.DropTable(
                name: "LoaiHangHoa");

            migrationBuilder.DropTable(
                name: "NhanVien");

            migrationBuilder.DropTable(
                name: "TaiXe");

            migrationBuilder.DropTable(
                name: "LinhVucKinhDoanh");

            migrationBuilder.DropTable(
                name: "LoaiChucVu");

            migrationBuilder.DropTable(
                name: "LoaiGiayPhepLaiXe");
        }
    }
}
