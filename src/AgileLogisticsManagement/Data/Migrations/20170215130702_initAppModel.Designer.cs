﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using AgileLogisticsManagement.Data;

namespace AgileLogisticsManagement.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170215130702_initAppModel")]
    partial class initAppModel
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("AgileLogisticsManagement.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.DoiTac", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CMND");

                    b.Property<string>("DiaChi");

                    b.Property<int>("IdLinhVucKinhDoanh");

                    b.Property<bool>("IsCaNhan");

                    b.Property<int?>("LinhVucKinhDoanhId");

                    b.Property<string>("LoaiHangHoaVanTai");

                    b.Property<string>("MaSo");

                    b.Property<string>("MaSoThue");

                    b.Property<string>("NguoiLienHe");

                    b.Property<string>("SoDienThoai");

                    b.Property<string>("Ten");

                    b.Property<string>("ThongTinXuatHoaDon");

                    b.HasKey("Id");

                    b.HasIndex("LinhVucKinhDoanhId");

                    b.ToTable("DoiTac");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.DonHangGhep", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("HopDongVanTaiId");

                    b.Property<int?>("HopDongVanTaiLeId");

                    b.Property<int>("IdHopDongVanTai");

                    b.Property<int>("IdHopDongVanTaiLe");

                    b.HasKey("Id");

                    b.HasIndex("HopDongVanTaiId");

                    b.HasIndex("HopDongVanTaiLeId");

                    b.ToTable("DonHangGhep");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.HopDongVanTai", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("DoiTacId");

                    b.Property<string>("FileHopDong");

                    b.Property<double>("GiaTriHopDong");

                    b.Property<int>("IdDoiTac");

                    b.Property<int>("IdLoaiHangHoa");

                    b.Property<int>("IdNhanVien");

                    b.Property<int>("IdTaiXe");

                    b.Property<double>("LePhi");

                    b.Property<int?>("LoaiHangHoaId");

                    b.Property<string>("MaHopDong");

                    b.Property<int?>("NhanVienId");

                    b.Property<string>("NoiDungEmail");

                    b.Property<int?>("TaiXeId");

                    b.Property<double>("Thue");

                    b.Property<string>("TrangThaiHopDong");

                    b.HasKey("Id");

                    b.HasIndex("DoiTacId");

                    b.HasIndex("LoaiHangHoaId");

                    b.HasIndex("NhanVienId");

                    b.HasIndex("TaiXeId");

                    b.ToTable("HopDongVanTai");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.HopDongVanTaiLe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("DoiTacId");

                    b.Property<int>("IdDoiTac");

                    b.Property<int>("IdLoaiHangHoa");

                    b.Property<int?>("LoaiHangHoaId");

                    b.HasKey("Id");

                    b.HasIndex("DoiTacId");

                    b.HasIndex("LoaiHangHoaId");

                    b.ToTable("HopDongVanTaiLe");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.LinhVucKinhDoanh", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Ten");

                    b.HasKey("Id");

                    b.ToTable("LinhVucKinhDoanh");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.LoaiChucVu", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Ten");

                    b.HasKey("Id");

                    b.ToTable("LoaiChucVu");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.LoaiGiayPhepLaiXe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Ten");

                    b.HasKey("Id");

                    b.ToTable("LoaiGiayPhepLaiXe");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.LoaiHangHoa", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Ten");

                    b.HasKey("Id");

                    b.ToTable("LoaiHangHoa");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.LoTrinhGiao", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DiaDiem");

                    b.Property<int?>("HopDongVanTaiId");

                    b.Property<int>("IdHopDongVanTai");

                    b.Property<string>("NguoiLienHe");

                    b.Property<string>("NguoiNhan");

                    b.Property<string>("SoDienThoai");

                    b.Property<DateTime>("ThoiGian");

                    b.HasKey("Id");

                    b.HasIndex("HopDongVanTaiId");

                    b.ToTable("LoTrinhGiao");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.NhanVien", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DiaChi");

                    b.Property<int>("IdLoaiChucVu");

                    b.Property<int?>("LoaiChucVuId");

                    b.Property<string>("MaSo");

                    b.Property<string>("SoDienThoai");

                    b.Property<string>("Ten");

                    b.HasKey("Id");

                    b.HasIndex("LoaiChucVuId");

                    b.ToTable("NhanVien");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.TaiXe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DiaChi");

                    b.Property<int>("IdLoaiGiayPhepLaiXe");

                    b.Property<int?>("LoaiGiayPhepLaiXeId");

                    b.Property<string>("MaSo");

                    b.Property<string>("SoDienThoai");

                    b.Property<string>("Ten");

                    b.HasKey("Id");

                    b.HasIndex("LoaiGiayPhepLaiXeId");

                    b.ToTable("TaiXe");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.TamUngTaiXe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("IdHopDongVanTai");

                    b.Property<int>("IdTaiXe");

                    b.Property<double>("SoTien");

                    b.Property<int?>("TaiXeId");

                    b.Property<DateTime>("ThoiGian");

                    b.HasKey("Id");

                    b.HasIndex("TaiXeId");

                    b.ToTable("TamUngTaiXe");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.Xe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BienSo");

                    b.Property<int>("IdTaiXe");

                    b.Property<DateTime>("KiemDinh_Han");

                    b.Property<DateTime>("KiemDinh_Ngay");

                    b.Property<string>("MaSo");

                    b.HasKey("Id");

                    b.ToTable("Xe");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.DoiTac", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.LinhVucKinhDoanh", "LinhVucKinhDoanh")
                        .WithMany("DoiTacs")
                        .HasForeignKey("LinhVucKinhDoanhId");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.DonHangGhep", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.HopDongVanTai", "HopDongVanTai")
                        .WithMany("DonHangGheps")
                        .HasForeignKey("HopDongVanTaiId");

                    b.HasOne("AgileLogisticsManagement.Models.HopDongVanTaiLe", "HopDongVanTaiLe")
                        .WithMany("DonHangGheps")
                        .HasForeignKey("HopDongVanTaiLeId");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.HopDongVanTai", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.DoiTac", "DoiTac")
                        .WithMany("HopDongVanTais")
                        .HasForeignKey("DoiTacId");

                    b.HasOne("AgileLogisticsManagement.Models.LoaiHangHoa", "LoaiHangHoa")
                        .WithMany("HopDongVanTais")
                        .HasForeignKey("LoaiHangHoaId");

                    b.HasOne("AgileLogisticsManagement.Models.NhanVien", "NhanVien")
                        .WithMany()
                        .HasForeignKey("NhanVienId");

                    b.HasOne("AgileLogisticsManagement.Models.TaiXe", "TaiXe")
                        .WithMany("HopDongVanTais")
                        .HasForeignKey("TaiXeId");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.HopDongVanTaiLe", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.DoiTac", "DoiTac")
                        .WithMany("HopDongVanTaiLes")
                        .HasForeignKey("DoiTacId");

                    b.HasOne("AgileLogisticsManagement.Models.LoaiHangHoa", "LoaiHangHoa")
                        .WithMany("HopDongVanTaiLes")
                        .HasForeignKey("LoaiHangHoaId");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.LoTrinhGiao", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.HopDongVanTai", "HopDongVanTai")
                        .WithMany("LoTrinhGiaos")
                        .HasForeignKey("HopDongVanTaiId");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.NhanVien", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.LoaiChucVu", "LoaiChucVu")
                        .WithMany("NhanViens")
                        .HasForeignKey("LoaiChucVuId");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.TaiXe", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.LoaiGiayPhepLaiXe", "LoaiGiayPhepLaiXe")
                        .WithMany("TaiXes")
                        .HasForeignKey("LoaiGiayPhepLaiXeId");
                });

            modelBuilder.Entity("AgileLogisticsManagement.Models.TamUngTaiXe", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.TaiXe", "TaiXe")
                        .WithMany("TamUngTaiXes")
                        .HasForeignKey("TaiXeId");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("AgileLogisticsManagement.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("AgileLogisticsManagement.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
