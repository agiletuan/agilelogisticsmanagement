﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AgileLogisticsManagement.Data.Migrations
{
    public partial class useraddmoreinfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DoiTac_LinhVucKinhDoanh_LinhVucKinhDoanhId",
                table: "DoiTac");

            migrationBuilder.DropIndex(
                name: "IX_DoiTac_LinhVucKinhDoanhId",
                table: "DoiTac");

            migrationBuilder.DropColumn(
                name: "LinhVucKinhDoanhId",
                table: "DoiTac");

            migrationBuilder.AddColumn<string>(
                name: "Fullname",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DoiTac_IdLinhVucKinhDoanh",
                table: "DoiTac",
                column: "IdLinhVucKinhDoanh");

            migrationBuilder.AddForeignKey(
                name: "FK_DoiTac_LinhVucKinhDoanh_IdLinhVucKinhDoanh",
                table: "DoiTac",
                column: "IdLinhVucKinhDoanh",
                principalTable: "LinhVucKinhDoanh",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DoiTac_LinhVucKinhDoanh_IdLinhVucKinhDoanh",
                table: "DoiTac");

            migrationBuilder.DropIndex(
                name: "IX_DoiTac_IdLinhVucKinhDoanh",
                table: "DoiTac");

            migrationBuilder.DropColumn(
                name: "Fullname",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<int>(
                name: "LinhVucKinhDoanhId",
                table: "DoiTac",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DoiTac_LinhVucKinhDoanhId",
                table: "DoiTac",
                column: "LinhVucKinhDoanhId");

            migrationBuilder.AddForeignKey(
                name: "FK_DoiTac_LinhVucKinhDoanh_LinhVucKinhDoanhId",
                table: "DoiTac",
                column: "LinhVucKinhDoanhId",
                principalTable: "LinhVucKinhDoanh",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
