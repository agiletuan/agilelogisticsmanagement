﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AgileLogisticsManagement.Data.Migrations
{
    public partial class createxeinhopdongvantai : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdXe",
                table: "HopDongVanTai",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_HopDongVanTai_IdXe",
                table: "HopDongVanTai",
                column: "IdXe");

            migrationBuilder.AddForeignKey(
                name: "FK_HopDongVanTai_Xe_IdXe",
                table: "HopDongVanTai",
                column: "IdXe",
                principalTable: "Xe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HopDongVanTai_Xe_IdXe",
                table: "HopDongVanTai");

            migrationBuilder.DropIndex(
                name: "IX_HopDongVanTai_IdXe",
                table: "HopDongVanTai");

            migrationBuilder.DropColumn(
                name: "IdXe",
                table: "HopDongVanTai");
        }
    }
}
