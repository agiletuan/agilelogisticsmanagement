﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AgileLogisticsManagement.Models;

namespace AgileLogisticsManagement.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);


            //Configure Column
            //one-to-many 
            builder.Entity<LoaiChucVu>()
                  .HasMany<NhanVien>(s => s.NhanViens)
                    .WithOne(s => s.LoaiChucVu)
                    .HasForeignKey(s => s.IdLoaiChucVu).IsRequired(true)
                    .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);
            //one-to-many 
            builder.Entity<LinhVucKinhDoanh>()
                  .HasMany<DoiTac>(s => s.DoiTacs)
                    .WithOne(s => s.LinhVucKinhDoanh)
                    .HasForeignKey(s => s.IdLinhVucKinhDoanh).IsRequired(true)
                    .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);
            //one-to-many 
            builder.Entity<Xe>()
                  .HasMany<HopDongVanTai>(s => s.HopDongVanTais)
                    .WithOne(s => s.Xe)
                    .HasForeignKey(s => s.IdXe).IsRequired(true)
                    .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);
            builder.Entity<LoaiHangHoa>()
                 .HasMany<HopDongVanTai>(s => s.HopDongVanTais)
                   .WithOne(s => s.LoaiHangHoa)
                   .HasForeignKey(s => s.IdLoaiHangHoa).IsRequired(true)
                   .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);
            builder.Entity<TaiXe>()
                 .HasMany<HopDongVanTai>(s => s.HopDongVanTais)
                   .WithOne(s => s.TaiXe)
                   .HasForeignKey(s => s.IdTaiXe).IsRequired(true)
                   .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);
            builder.Entity<NhanVien>()
                 .HasMany<HopDongVanTai>(s => s.HopDongVanTais)
                   .WithOne(s => s.NhanVien)
                   .HasForeignKey(s => s.IdNhanVien).IsRequired(true)
                   .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);
            //one-to-many 
            builder.Entity<LoaiGiayPhepLaiXe>()
                  .HasMany<TaiXe>(s => s.TaiXes)
                    .WithOne(s => s.LoaiGiayPhepLaiXe)
                    .HasForeignKey(s => s.IdLoaiGiayPhepLaiXe).IsRequired(true)
                    .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);
            //one-to-many 
            builder.Entity<TaiXe>()
                  .HasMany<TamUngTaiXe>(s => s.TamUngTaiXes)
                    .WithOne(s => s.TaiXe)
                    .HasForeignKey(s => s.IdTaiXe).IsRequired(true)
                    .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);

            //one-to-many             
            builder.Entity<LoaiHangHoa>()
                 .HasMany<HopDongVanTaiLe>(s => s.HopDongVanTaiLes)
                   .WithOne(s => s.LoaiHangHoa)
                   .HasForeignKey(s => s.IdLoaiHangHoa).IsRequired(true)
                   .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);
            
            builder.Entity<DoiTac>()
                 .HasMany<HopDongVanTaiLe>(s => s.HopDongVanTaiLes)
                   .WithOne(s => s.DoiTac)
                   .HasForeignKey(s => s.IdDoiTac).IsRequired(true)
                   .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);


            //one-to-many             
            builder.Entity<HopDongVanTai>()
                 .HasMany<LoTrinhGiao>(s => s.LoTrinhGiaos)
                   .WithOne(s => s.HopDongVanTai)
                   .HasForeignKey(s => s.IdHopDongVanTai).IsRequired(true)
                   .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);

            //one-to-many             
            builder.Entity<HopDongVanTai>()
                 .HasMany<DonHangGhep>(s => s.DonHangGheps)
                   .WithOne(s => s.HopDongVanTai)
                   .HasForeignKey(s => s.IdHopDongVanTai).IsRequired(true)
                   .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);
            
             //one-to-many             
            builder.Entity<HopDongVanTaiLe>()
                 .HasMany<DonHangGhep>(s => s.DonHangGheps)
                   .WithOne(s => s.HopDongVanTaiLe)
                   .HasForeignKey(s => s.IdHopDongVanTaiLe).IsRequired(true)
                   .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.Restrict);


        }

        public DbSet<LinhVucKinhDoanh> LinhVucKinhDoanhs { get; set; }
        public DbSet<DoiTac> DoiTacs { get; set; }
        public DbSet<LoaiHangHoa> LoaiHangHoas { get; set; }
        public DbSet<HopDongVanTai> HopDongVanTais { get; set; }
        public DbSet<LoTrinhGiao> LoTrinhGiaos { get; set; }
        public DbSet<DonHangGhep> DonHangGheps { get; set; }
        public DbSet<HopDongVanTaiLe> HopDongVanTaiLes { get; set; }

        public DbSet<LoaiChucVu> LoaiChucVus { get; set; }
        public DbSet<NhanVien> NhanViens { get; set; }

        public DbSet<LoaiGiayPhepLaiXe> LoaiGiayPhepLaiXes { get; set; }
        public DbSet<TaiXe> TaiXes { get; set; }
        public DbSet<TamUngTaiXe> TamUngTaiXes { get; set; }
        public DbSet<Xe> Xes { get; set; }



    }
}
